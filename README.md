# udiskie.el - Emacs interface for udiskie

[![language](https://img.shields.io/badge/elisp-green.svg)]()
[![version](https://img.shields.io/badge/version-1.0.0-green.svg)]()


## Usage

Use one of the autoloaded interactive functions:

- `udiskie-disconnect-USB-disk`
- `udiskie-mount-USB-disk`
- `udiskie-cd-to-USB-disk`


