;;; udiskie.el --- Emacs Interface for udiskie -*- lexical-binding: t -*-

;; Copyright (C) 2020 Arnaud Hoffmann

;; Author: Arnaud Hoffmann <tuedachu@gmail.com>
;; Maintainer: Arnaud Hoffmann <tuedachu@gmail.com>
;; URL: https://gitlab.com/tuedachu/udiskie.el
;; Version: 1.0.0
;; Package-Requires: ((emacs "24.3"))
;; Keywords: tools, unix, convenience, files, hardware

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; This program is an Emacs interface for the set of tools provided by
;; udiskie.  It enables the user to mount/unmount connected disks from
;; within Emacs.
;;
;; udiskie is a udisks2 front-end that allows to manage removeable
;; media such as CDs or flash drives from userspace.  More
;; information: https://github.com/coldfix/udiskie
;;
;; Setup:
;; Install the package (M-x package-install) and use one of the autoloaded functions:
;; - udiskie-disconnect-USB-disk
;; - udiskie-mount-USB-disk
;;
;;; Code:

(require 'eshell)

(defvar udiskie-version
  "1.0.0"
  "Current version of `udiskie'.")

(defvar udiskie--message-prompt
  "[udiskie.el] "
  "String starting all udiskie.el messages.")

(defun udiskie--message (msg)
  "Format and display MSG at the bottom of the screen."
  (message (concat udiskie--message-prompt
                   msg)))

(defun udiskie--program-exists-p (program)
  "Check if PROGRAM is intalled.

Returns t if PROGRAM is installed on the machine.  Else, returns
nil."
  (not (if (not (executable-find program))
           (udiskie--message (concat program
                                     " cannot be found.")))))

(defun udiskie--get-disk-list ()
  "Get the list of all connected disks."
  (with-temp-buffer
    (apply #'call-process "lsblk" nil t nil
           '("-l"))
    (buffer-string)))


(defun udiskie--get-list-of-mounted-disks ()
  "Get the list of removable mounted disks.

Queries 'lsblk' the list of devices.

Return a list of alist of ,ounted disks that have mount point different
thant '/' and '/boot/'.

Each alist has the following structure:
'((name .  <name>) (size . <size>) (mountpoint <mountpoint>))"
  (when (udiskie--program-exists-p "lsblk")
    (let* ((output-str (udiskie--get-disk-list))
           (output (split-string output-str "\n" t nil))
           (partition-list '()))
      (dolist (line output)
        (let ((fields (split-string line
                                    "[ \t]"
                                    t
                                    nil)))
          (when (and (string= (nth 5 fields) "part")
                     (nth 6 fields)
                     (not (or (string= (nth 6 fields)
                                       "/boot")
                              (string= (nth 6 fields)
                                       "/"))))
            (setq partition-list (append partition-list
                                         `(((name . ,(nth 0 fields))
                                            (size . ,(nth 3 fields))
                                            (mountpoint .  ,(mapconcat #'identity
                                                                       (nthcdr 6 fields)
                                                                       " ")))))))))
      partition-list)))


(defun udiskie--get-list-of-umounted-disks ()
  "Get the list of connected disks that are not mounted.

Queries 'lsblk' the list of devices.

Return an  alist of unmounted connected disks.

Each alist has the following structure:
'((name .  <name>) (size . <size>))"
  (when (udiskie--program-exists-p "lsblk")
    (let* ((output-str (udiskie--get-disk-list))
           (output (split-string output-str "\n" t nil))
           (partition-list '()))
      (dolist (line output)
        (let ((fields (split-string line
                                    "[ \t]"
                                    t
                                    nil)))
          (when (and (string= (nth 5 fields) "part")
                     (not (nth 6 fields))
                     (setq partition-list (append partition-list
                                                  `(((name . ,(nth 0 fields))
                                                     (size . ,(nth 3 fields))))))))))
      partition-list)))


;;;###autoload
(defun udiskie-disconnect-USB-disk ()
  "Disconnect USB disk among the connected removable disks."
  (interactive)
  (when (udiskie--program-exists-p "udiskie-umount")
    (let ((partition-list (udiskie--get-list-of-mounted-disks)))
      (if (not partition-list)
          (udiskie--message "No USB disk connected...")
        (let* ((disk (completing-read (concat udiskie--message-prompt
                                              "Choose a USB disk to disconnect:")
                                      (mapcar (lambda (element)
                                                (concat (file-name-nondirectory (cdr (assoc 'mountpoint element)))
                                                        " - "
                                                        (cdr (assoc 'size element))
                                                        " - "
                                                        (cdr (assoc 'name element))
                                                        " - "
                                                        (cdr (assoc 'mountpoint element))))
                                              partition-list)))
               (mountpoint (nth 3 (split-string disk " - " t nil))))
          (if (string-match "failed to unmount"
                            (with-temp-buffer
                              (apply #'call-process "udiskie-umount" nil t nil
                                     `(,mountpoint))
                              (buffer-string)))
              (udiskie--message (concat "Failed to unmount "
                                        mountpoint))
            (message (concat udiskie--message-prompt
                             disk
                             " unmounted."))))))))


;;;###autoload
(defun udiskie-mount-USB-disk ()
  "Mount a connected USB disk."
  (interactive)
  (when (udiskie--program-exists-p "udiskie-mount")
    (let ((partition-list (udiskie--get-list-of-umounted-disks)))
      (if (not partition-list)
          (udiskie--message "No USB disk connected...")
        (let* ((disk (completing-read (concat udiskie--message-prompt
                                              "Choose a USB disk to mount:")
                                      (mapcar (lambda (element)
                                                (concat (cdr (assoc 'name element))
                                                        "("
                                                        (cdr (assoc 'size element))
                                                        ")"))
                                              partition-list)))
               (device-name (concat "/dev/"
                                    (nth 0 (split-string disk "(" t nil))))
               (udiskie-output (with-temp-buffer
                                 (apply #'call-process "udiskie-mount" nil t nil
                                        `(,device-name))
                                 (buffer-string)))
               (mount-success? (string-match "mounted" udiskie-output))
               (mountpoint (nth 3 (split-string udiskie-output))))
          (if (not mount-success?)
              (udiskie--message (concat "Failed mounting "
                                        device-name))
            (udiskie--message (concat device-name
                                      " mounted on "
                                      mountpoint))
            (when (y-or-n-p "Do you want to open eshell at that mountpoint? ")
              (let ((eshell-buffer-name "*udiskie.el*"))
                (eshell)
                (when (eshell-interactive-process)
                  (eshell t))
                (eshell-interrupt-process)
                (insert (concat "cd "
                                (shell-quote-argument mountpoint)))

                (eshell-send-input)))))))))


;;;###autoload
(defun udiskie-cd-to-USB-disk ()
  "cd to USB disk"
  (interactive)
  (when (udiskie--program-exists-p "udiskie-umount")
    (let ((partition-list (udiskie--get-list-of-mounted-disks)))
      (if (not partition-list)
          (udiskie--message "No USB disk connected...")
        (let* ((disk (completing-read (concat udiskie--message-prompt
                                              "Choose a USB disk to go to:")
                                      (mapcar (lambda (element)
                                                (concat (file-name-nondirectory (cdr (assoc 'mountpoint element)))
                                                        " - "
                                                        (cdr (assoc 'size element))
                                                        " - "
                                                        (cdr (assoc 'name element))
                                                        " - "
                                                        (cdr (assoc 'mountpoint element))))
                                              partition-list)))
               (mountpoint (nth 3 (split-string disk " - " t nil)))
               (bufs (cl-loop for b in (mapcar 'buffer-name (buffer-list))
                              when (helm-ff--eshell-interactive-buffer-p b)
                              collect b))
               (eshell-buffer-name (if (cdr bufs)
                                       (helm-comp-read "Switch to eshell buffer: " bufs
                                                       :must-match t)
                                     (car bufs))))
          (switch-to-buffer eshell-buffer-name)
          (eshell/cd mountpoint)
          (eshell-reset))))))


(provide 'udiskie)
;;; udiskie.el ends here
